package com.phoenixshoi.airplane;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.phoenixshoi.airplane.engine.SaveEngine;
import com.phoenixshoi.airplane.states.GameStateManager;
import com.phoenixshoi.airplane.states.MenuState;
import com.phoenixshoi.airplane.states.SettingState;
import com.phoenixshoi.airplane.states.StartScreenState;

import java.awt.Color;

public class MainClass extends ApplicationAdapter {

	private GameStateManager gsm;
	private  SpriteBatch batch;
	private SaveEngine save;

	@Override
	public void create () {
		batch = new SpriteBatch();
		gsm = new GameStateManager();
		Gdx.gl.glClearColor(0, 0, 0, 1);
		save=new SaveEngine();
		gsm.push(new StartScreenState(gsm,save,Gdx.graphics.getHeight(),Gdx.graphics.getWidth()));
		//gsm.push(new MenuState(gsm,save,Gdx.graphics.getHeight(),Gdx.graphics.getWidth()));

	}

	@Override
	public void render () {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		gsm.update(Gdx.graphics.getDeltaTime());
		gsm.render(batch);
	}

	@Override
	public void resize(int width,int height){

		gsm.resume();
	}

	@Override
	public void pause(){

	}

	@Override
	public void resume(){

		gsm.resume();
	}

	@Override
	public void dispose(){

	}
}
