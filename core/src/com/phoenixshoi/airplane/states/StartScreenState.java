package com.phoenixshoi.airplane.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.phoenixshoi.airplane.engine.SaveEngine;
import com.phoenixshoi.airplane.sprites.Background;

/**
 * Created by phoenixshoi on 21.08.16.
 */
public class StartScreenState extends State{
    private int screenWight;
    private int screenHeight;

    Sprite dartoHoodLogo;
    Texture dartoHoodLogoTexture;

    SaveEngine save;

    int time=200;

    public StartScreenState(GameStateManager gsm,SaveEngine saveEngine,int stateHeight, int stateWidth){
        super(gsm,stateHeight,stateWidth);

        screenWight = stateWidth;
        screenHeight = stateHeight;

        save = saveEngine;

        camera.setToOrtho(false,screenWight,screenHeight);

        dartoHoodLogoTexture = new Texture(Gdx.files.internal("Backgrounds/Logo/dhLogo.png"));
        dartoHoodLogo = new Sprite(dartoHoodLogoTexture);

        dartoHoodLogo.setSize(dartoHoodLogo.getWidth()/2*scaleConstW,dartoHoodLogo.getHeight()/2*scaleConstH);

        dartoHoodLogo.setPosition(screenWight/2-dartoHoodLogo.getWidth()/2,screenHeight/2-dartoHoodLogo.getHeight()/2);

    }

    @Override
    protected void handleInput() {

    }

    @Override
    public void update(float dt) {
        if(time>0){
            time--;
        }
        else {
            gsm.push(new MenuState(gsm,save,screenHeight,screenWight));
            gsm.startMusic();
        }
    }

    @Override
    public void render(SpriteBatch sb) {
        camera.update();

        sb.setProjectionMatrix(camera.combined);

        sb.begin();

        dartoHoodLogo.draw(sb);

        sb.end();
    }

    @Override
    public void dispose() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void resetting() {

    }
}
