package com.phoenixshoi.airplane.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.phoenixshoi.airplane.element.ButtonGame;
import com.phoenixshoi.airplane.engine.DamageEngine;
import com.phoenixshoi.airplane.engine.SaveEngine;
import com.phoenixshoi.airplane.sprites.Airplane;
import com.phoenixshoi.airplane.sprites.Background;
import com.phoenixshoi.airplane.sprites.Shot;

import java.util.ArrayList;

/**
 * Created by phoenixshoi on 24.06.16.
 */

public class PlayState extends State {

    private ArrayList<Airplane> airplanes;
    private ArrayList<Shot> shots;


    private int screenWight;
    private int screenHeight;

    private int realWight;
    private int realHeight;

    private int sense=0;
    private int senseAi=0;

    private int senseMax=2;

    private DamageEngine damageEngine;

    private int deadTime=70;
    private int backButtonTime=15;

    private ButtonGame pauseButton;

    SaveEngine save;

    public PlayState(GameStateManager gsm,SaveEngine save,int playerModelNumber,int scoreMax,int stateHeight,int stateWidth,int gameHeight,int gameWidth) {
        super(gsm,stateHeight,stateWidth);

        this.save=save;


        airplanes = new ArrayList<Airplane>();
        shots = new ArrayList<Shot>();

        screenWight = 800;
        screenHeight = 480;

        realWight = stateWidth;
        realHeight = stateHeight;

        background = new Background(screenWight,screenHeight,true,1,1);
        background.setScoreMax(scoreMax);

        airplanes.add(new Airplane((screenWight/8)*2-32,background.getHeightAirplane(),true,playerModelNumber,0,save.getVibrateValue(),save.getSoundValue(),screenHeight,screenWight,background.getHeightAirplane()));
        airplanes.add(new Airplane(screenWight-(screenWight/8)*2,background.getHeightAirplane(),false,MathUtils.random(1,4),1,save.getVibrateValue(),save.getSoundValue(),screenHeight,screenWight,background.getHeightAirplane()));

        damageEngine= new DamageEngine();

        resetting();
    }

    @Override
    protected void handleInput() {

        if(pauseButton.checkClick(Gdx.input.getX(),realHeight - Gdx.input.getY())){
            gsm.stopFly();
            gsm.set(new PauseState(gsm,this,save,realHeight,realWight));
            backButtonTime=15;
            return;
        }


        if ((((Gdx.input.isTouched(0))&&(!Gdx.input.isTouched(1)))||((!Gdx.input.isTouched(0))&&(Gdx.input.isTouched(1))))&&(sense==0)) {
            int realX;
            if(Gdx.input.isTouched(1))
                realX=Gdx.input.getX(1);
            else
                realX=Gdx.input.getX(0);

            if(realX<realWight/2){
                airplanes.get(0).setLeft();
                sense=senseMax;
            }
            else{
                airplanes.get(0).setRight();
                sense=senseMax;
            }
        }else{
            if(sense>0)
                sense--;
        }


        if ((Gdx.input.isTouched(0))&&(Gdx.input.isTouched(1))) {

            if (airplanes.get(0).acceptShot()) {
                shots.add(airplanes.get(0).createShot());
                Gdx.input.vibrate(save.getVibrateValue());
                gsm.playShot();
            }

        }

        if(senseAi==0) {
            airplanes.get(1).aiControl(airplanes.get(0), shots,gsm);
            senseAi=senseMax;
        }
        else {
            if(senseAi>0){
                senseAi--;
            }
        }

        if((backButtonTime==0)&&(Gdx.input.isKeyPressed(Input.Keys.BACK))){
            gsm.stopFly();
            gsm.set(new PauseState(gsm,this,save,realHeight,realWight));
            backButtonTime=10;
        }

        if(backButtonTime>0){

            backButtonTime--;
        }


    }

    @Override
    public void update(float dt) {


        if(background.getTheEnd()){
            if(deadTime==0){
                gsm.set(new MenuState(gsm,save,realHeight,realWight));
                gsm.stopFly();
            }
            else
                deadTime--;
        }
        else {



            for (int i = 0; i < shots.size(); i++) {
                shots.get(i).update(dt, background.getHouses());
                if (shots.get(i).getShotLife() <= 0) {
                    shots.remove(i);
                    i--;
                }
            }

            for (Airplane airplane : airplanes) {
                airplane.update(dt, background.getHouses(),gsm);
            }

            int winNum = damageEngine.checkDamage(shots, airplanes, background.getScore());

            background.checkWin(winNum,airplanes.get(0).getColorID(),airplanes.get(1).getColorID());
            handleInput();
        }

        background.updateBackground(dt);

    }


    @Override
    public void render(SpriteBatch sb) {
        camera.update();

        sb.setProjectionMatrix(camera.combined);

        sb.begin();

        background.drawBackground(sb);


        for (Shot shot : shots){
            shot.getShot().draw(sb);
        }

        for (Airplane airplane : airplanes){
            airplane.getAirplane().draw(sb);
            /*
            if(airplane.getId()==1)
                airplane.getAr().draw(sb);
                */
        }

        pauseButton.getSpriteButton().draw(sb);

        sb.end();
    }

    @Override
    public void dispose() {

    }

    @Override
    public void resume() {
        background.resume();
        for (Shot shot : shots){
            shot.resume();
        }

        for (Airplane airplane : airplanes){
            airplane.resume();
        }
    }

    @Override
    public void resetting() {
        camera.setToOrtho(false,screenWight,screenHeight);
        pauseButton= new ButtonGame(screenWight/2,screenHeight-19,realWight/2,realHeight-19,"Buttons/pauseButton.png",save.getVibrateValue(),false,1,1);
    }

}
