package com.phoenixshoi.airplane.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.phoenixshoi.airplane.element.ButtonGame;
import com.phoenixshoi.airplane.element.ControlGame;
import com.phoenixshoi.airplane.engine.SaveEngine;
import com.phoenixshoi.airplane.sprites.Background;

import org.lwjgl.Sys;

/**
 * Created by phoenixshoi on 20.07.16.
 */
public class SettingState extends State {

    private int screenWight;
    private int screenHeight;

    private ControlGame musicControl;
    private ControlGame soundControl;
    private ControlGame vibrateControl;

    private boolean musicSaveFlag=false;

    private State saveState;

    private SaveEngine save;


    public SettingState(GameStateManager gsm, SaveEngine save,State saveState, Background background,int stateHeight,int stateWidth) {
        super(gsm,stateWidth,stateHeight);

        this.save=save;

        this.saveState=saveState;

        screenWight = stateWidth;
        screenHeight = stateHeight;

        this.background = background;
        this.background.logoOff();

        //camera.setToOrtho(false, screenWight, screenHeight);

        musicControl = new ControlGame(screenHeight-screenHeight/4,"String/SettingString/musicString.png",0,screenHeight,screenWight,scaleConstH,scaleConstW);
        soundControl = new ControlGame(screenHeight/2,"String/SettingString/soundString.png",0,screenHeight,screenWight,scaleConstH,scaleConstW);
        vibrateControl = new ControlGame(screenHeight/4,"String/SettingString/vibrateString.png",0,screenHeight,screenWight,scaleConstH,scaleConstW);

        musicControl.setValue(save.getMusicValue());
        soundControl.setValue(save.getSoundValue());
        vibrateControl.setValue(save.getVibrateValue());

    }

    @Override
    protected void handleInput() {

        int inputX = 0;
        int inputY = 0;

        if (Gdx.input.isTouched()) {
            inputX = Gdx.input.getX();
            inputY = screenHeight - Gdx.input.getY();
        }
        else {
            musicControl.setOff();
            soundControl.setOff();
            vibrateControl.setOff();
        }

        if((!soundControl.getTouchFlag())&&(!vibrateControl.getTouchFlag())) {
            musicControl.input(inputX, inputY);
            gsm.setVolume(musicControl.math()/100f);
            save.saveParam(musicControl.math(),soundControl.math(),vibrateControl.math(),save.getLastPointValue(),save.getSaveAirplaneModelValue(),save.getHelpSkipValue(),save.getControlVersionValue());

        }

        if((!musicControl.getTouchFlag())&&(!vibrateControl.getTouchFlag())) {
            soundControl.input(inputX, inputY);
            gsm.setFlyVolume(soundControl.math()/100f);
            save.saveParam(musicControl.math(), soundControl.math(), vibrateControl.math(), save.getLastPointValue(), save.getSaveAirplaneModelValue(), save.getHelpSkipValue(),save.getControlVersionValue());
        }
        if((!soundControl.getTouchFlag())&&(!musicControl.getTouchFlag())) {
            vibrateControl.input(inputX, inputY);
            save.saveParam(musicControl.math(), soundControl.math(), vibrateControl.math(), save.getLastPointValue(), save.getSaveAirplaneModelValue(), save.getHelpSkipValue(),save.getControlVersionValue());
        }

        Gdx.input.setCatchBackKey(true);
        if(Gdx.input.isKeyPressed(Input.Keys.BACK)){
            save.saveParam(musicControl.math(),soundControl.math(),vibrateControl.math(),save.getLastPointValue(),save.getSaveAirplaneModelValue(),save.getHelpSkipValue(),save.getControlVersionValue());
            saveState.resetting();
            gsm.set(saveState);
        }
        musicSaveFlag=musicControl.getTouchFlag();
    }

    @Override
    public void update(float dt) {

        handleInput();

        background.updateBackground(dt);

    }

    @Override
    public void render(SpriteBatch sb) {
        sb.begin();
        background.drawBackground(sb);
        musicControl.draw(sb);
        soundControl.draw(sb);
        vibrateControl.draw(sb);

        sb.end();
    }

    @Override
    public void dispose() {

    }

    @Override
    public void resume() {
        background.resume();

    }

    @Override
    public void resetting() {

    }
}