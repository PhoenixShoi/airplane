package com.phoenixshoi.airplane.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.phoenixshoi.airplane.sprites.Background;

/**
 * Created by phoenixshoi on 24.06.16.
 */
public abstract class State {

    protected OrthographicCamera camera;
    protected Vector3 mouse;
    protected GameStateManager gsm;
    protected Background background;

    protected int scaleConstH;
    protected int scaleConstW;


    public  State(GameStateManager gsm,int stateHeight,int stateWidth){
        this.gsm = gsm;

        camera = new OrthographicCamera();
        mouse = new Vector3();

        scaleConstW = Math.max(800,stateWidth)/Math.min(800,stateWidth);
        scaleConstH = Math.max(480,stateHeight)/Math.min(480,stateHeight);


    }

    protected abstract  void handleInput();
    public abstract  void update(float dt);
    public abstract  void render(SpriteBatch sb);
    public abstract  void dispose();

    public abstract void resume();

    public abstract void resetting();
}
