package com.phoenixshoi.airplane.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.phoenixshoi.airplane.element.ButtonGame;
import com.phoenixshoi.airplane.element.ControlGame;
import com.phoenixshoi.airplane.engine.SaveEngine;
import com.phoenixshoi.airplane.sprites.Background;

/**
 * Created by phoenixshoi on 21.07.16.
 */
public class SelectAirplaneState extends State {

    private int screenWight;
    private int screenHeight;

    private SaveEngine save;

    private ControlGame lastPoint;

    private Texture chooseAirplaneStringTexture;
    private Sprite chooseAirplaneString;

    private Texture airplaneTexture;
    private Sprite airplane;

    private ButtonGame buttonLeft;
    private ButtonGame buttonRight;
    private ButtonGame playButton;

    private int airplaneX;
    private int airplaneY;
    private int airplaneNumber;

    public SelectAirplaneState(GameStateManager gsm, Background background, SaveEngine save,int stateHeight,int stateWidth){
        super(gsm,stateHeight,stateWidth);
        System.err.println(save.getLastPointValue());

        screenWight = stateWidth;
        screenHeight = stateHeight;

        //camera.translate(stateHeight,stateWidth);
        camera.setToOrtho(false,screenWight,screenHeight);

        this.background = background;
        background.logoOff();
        this.save = save;

        lastPoint = new ControlGame(screenHeight/3,"String/lastPointString.png",1,screenHeight,screenWight,scaleConstH,scaleConstW);
        lastPoint.setValue(save.getLastPointValue());

        chooseAirplaneStringTexture = new Texture("String/chooseAirplaneString.png");
        chooseAirplaneString = new Sprite(chooseAirplaneStringTexture);

        chooseAirplaneString.setSize((chooseAirplaneString.getWidth()/4)*scaleConstW,(chooseAirplaneString.getHeight()/4)*scaleConstH);
        chooseAirplaneString.setPosition(screenWight/2-chooseAirplaneString.getWidth()/2,screenHeight-screenHeight/4);

        buttonLeft = new ButtonGame(screenWight/2-40*scaleConstW, (int) (chooseAirplaneString.getY()-55*scaleConstH),screenWight/2-40*scaleConstW, (int) (chooseAirplaneString.getY()-55*scaleConstH),"Buttons/selectButton.png",save.getVibrateValue(),false,scaleConstH,scaleConstW);
        buttonRight = new ButtonGame(screenWight/2+40*scaleConstW,(int) (chooseAirplaneString.getY()-55*scaleConstH),screenWight/2+40*scaleConstW,(int) (chooseAirplaneString.getY()-55*scaleConstH),"Buttons/selectButton.png",save.getVibrateValue(),true,scaleConstH,scaleConstW);

        airplaneX=screenWight/2-8*scaleConstW;
        airplaneY=(int) (chooseAirplaneString.getY()-60*scaleConstH);

        setAirplaneModel(save.getSaveAirplaneModelValue());

        playButton = new ButtonGame(screenWight/2,screenHeight/10,screenWight/2,screenHeight/10,"Buttons/playButton.png",save.getVibrateValue(),false,scaleConstH,scaleConstW);

    }

    private void setAirplaneModel(int airplaneModelNumber){
        airplaneNumber=airplaneModelNumber;
        if(airplaneNumber>4){
            airplaneNumber=1;
        }
        if(airplaneNumber<1){
            airplaneNumber=4;
        }
        airplaneTexture = new Texture("AirplaneModels/airplaneModel"+airplaneNumber+".png");
        airplane = new Sprite(airplaneTexture);
        airplane.setSize(airplane.getWidth()*scaleConstW,airplane.getHeight()*scaleConstH);
        airplane.setPosition(airplaneX-airplane.getHeight()/2,airplaneY);
    }

    @Override
    protected void handleInput() {
        int inputX = 0;
        int inputY = 0;

        if (Gdx.input.isTouched()) {
            inputX = Gdx.input.getX();
            inputY = screenHeight - Gdx.input.getY();
        }
        else {
            lastPoint.setOff();
        }

        if((!buttonLeft.checkStatusButton())&&(!lastPoint.getTouchFlag())&&(!playButton.checkStatusButton())&&(buttonRight.checkClick(inputX,inputY))){
            setAirplaneModel(airplaneNumber-1);
        }

        if((!buttonRight.checkStatusButton())&&(!lastPoint.getTouchFlag())&&(!playButton.checkStatusButton())&&(buttonLeft.checkClick(inputX,inputY))){
            setAirplaneModel(airplaneNumber+1);
        }

        if((!buttonLeft.checkStatusButton())&&(!buttonRight.checkStatusButton())&&(!playButton.checkStatusButton())){
            lastPoint.input(inputX,inputY);
            System.err.println(lastPoint.math());
        }

        if((!buttonLeft.checkStatusButton())&&(!buttonRight.checkStatusButton())&&(!lastPoint.getTouchFlag())&&(playButton.checkClick(inputX,inputY))) {
            save.saveParam(save.getMusicValue(),save.getSoundValue(),save.getVibrateValue(),lastPoint.math(),airplaneNumber,save.getHelpSkipValue(),save.getControlVersionValue());
            gsm.set(new PlayState(gsm,save,airplaneNumber,save.getLastPointValue(),screenHeight,screenWight,screenHeight,screenWight));
        }


        if(Gdx.input.isKeyPressed(Input.Keys.BACK)){
            save.saveParam(save.getMusicValue(),save.getSoundValue(),save.getVibrateValue(),lastPoint.math(),airplaneNumber,save.getHelpSkipValue(),save.getControlVersionValue());

            Gdx.input.setCatchBackKey(true);
            background.logoOn();
            gsm.set(new MenuState(gsm,save,background,screenHeight,screenWight));

        }

    }

    @Override
    public void update(float dt) {
        handleInput();
        background.updateBackground(dt);
        gsm.stopFly();

    }

    @Override
    public void render(SpriteBatch sb) {

        camera.update();

        sb.setProjectionMatrix(camera.combined);

        sb.begin();
        background.drawBackground(sb);
        lastPoint.draw(sb);
        chooseAirplaneString.draw(sb);
        buttonLeft.getSpriteButton().draw(sb);
        buttonRight.getSpriteButton().draw(sb);
        airplane.draw(sb);
        playButton.getSpriteButton().draw(sb);
        sb.end();
    }

    @Override
    public void dispose() {

    }

    @Override
    public void resume() {
        background.resume();
    }

    @Override
    public void resetting() {

    }
}
