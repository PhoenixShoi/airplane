package com.phoenixshoi.airplane.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.phoenixshoi.airplane.engine.SaveEngine;
import com.phoenixshoi.airplane.sprites.Airplane;
import com.phoenixshoi.airplane.sprites.Background;
import com.phoenixshoi.airplane.sprites.Clock;
import com.phoenixshoi.airplane.sprites.House;
import com.phoenixshoi.airplane.sprites.Shot;

import java.util.ArrayList;

/**
 * Created by phoenixshoi on 20.07.16.
 */

public class HelpState extends State {

    private SaveEngine saveEngine;

    private int screenWight;
    private int screenHeight;

    private int realH;
    private int realW;

    private Clock clock;
    private int timeClock=25;

    private Airplane airplane;

    private Texture handTexture;
    private Sprite handLeft;
    private Sprite handRight;

    private Texture textTexture;
    private Sprite text;

    private Texture moveTexture;
    private Sprite move;

    private int stepHelp=1;

    private int blinkMove=20;
    private boolean moveFlag=true;

    private int sense=0;
    private int senseMax=2;

    private ArrayList<Shot> shots;



    public HelpState(GameStateManager gsm, SaveEngine save, int stateHeight, int stateWidth){
        super(gsm,stateHeight,stateWidth);

        screenWight = 800;
        screenHeight = 480;

        realH=stateHeight;
        realW=stateWidth;


        saveEngine = save;

        this.background=new Background(screenWight,screenHeight,false,scaleConstH,scaleConstW);
        background.houseOff();

        clock = new Clock(screenWight/2,screenHeight-screenHeight/4);
        clock.hideOn();

        airplane = new Airplane(screenWight/8,screenHeight/2,true,1,0,save.getVibrateValue(),save.getSoundValue(),screenHeight,screenWight,background.getHeightAirplane());


        handTexture = new Texture("HelpModel/hand.png");

        handLeft = new Sprite(handTexture);
        handLeft.flip(true,false);
        handLeft.rotate(-30);
        handLeft.setPosition(screenWight/4-handLeft.getWidth()/2,screenHeight/2-handLeft.getHeight());
        handRight = new Sprite(handTexture);
        handRight.rotate(30);
        handRight.setPosition(screenWight-screenWight/4-handRight.getWidth()/2,screenHeight/2-handLeft.getHeight());

        moveTexture = new Texture("HelpModel/left.png");
        move = new Sprite(moveTexture);
        move.rotate(-15);


        shots = new ArrayList<Shot>();

        textUpdate();


        resetting();


    }

    private void textUpdate(){
        textTexture = new Texture("String/HelpString/"+stepHelp+".png");
        text = new Sprite(textTexture);
        text.setPosition(screenWight/2-text.getWidth()/2,screenHeight-screenHeight/8);
    }

    @Override
    protected void handleInput() {
        int inputX = -1;
        int inputXSecond= -1;

        if(sense==0) {
            if (Gdx.input.isTouched(0)) {
                inputX = Gdx.input.getX(0);
            }
            if(Gdx.input.isTouched(1)){
                inputXSecond=Gdx.input.getX(1);
            }
            sense=senseMax;
        }
        else
            sense--;

        if(Gdx.input.isKeyPressed(Input.Keys.BACK)){
            Gdx.input.setCatchBackKey(true);
            background.logoOn();
            gsm.set(new MenuState(gsm,saveEngine,realH,realW));
            gsm.stopFly();
        }

        if (stepHelp == 1) {
            if (inputX > screenWight / 2) {
                clock.hideOff();
                airplane.setLeft();
                if (timeClock == 0) {
                    timeClock = 25;
                    stepHelp = 2;
                    clock.hideOn();
                    textUpdate();
                    moveTexture = new Texture("HelpModel/right.png");
                    move = new Sprite(moveTexture);
                    airplane = new Airplane(screenWight/8,screenHeight/2,true,1,0,saveEngine.getVibrateValue(),saveEngine.getSoundValue(),screenHeight,screenWight,background.getHeightAirplane());

                } else {
                    timeClock--;
                    clock.nextClock();
                }
            } else {
                if(sense==senseMax){
                    clock.hideOff();
                    timeClock = 25;
                }
            }
            return;
        }

        if (stepHelp == 2) {
            if ((inputX>0)&&(inputX < screenWight / 2)) {
                clock.hideOff();
                airplane.setRight();
                if (timeClock == 0) {
                    timeClock = 25;
                    stepHelp = 3;
                    clock.hideOn();
                    textUpdate();
                    moveFlag=false;
                    airplane = new Airplane(screenWight/8,screenHeight/2,true,1,0,saveEngine.getVibrateValue(),saveEngine.getSoundValue(),screenHeight,screenWight,background.getHeightAirplane());
                    airplane.startEngine();
                } else {
                    timeClock--;
                    clock.nextClock();
                }
            } else {
                if(sense==senseMax){
                    clock.hideOff();
                    timeClock = 25;
                }
            }
            return;
        }

        if (stepHelp == 3) {
            if((((inputX>0)&&(inputX < screenWight / 2))&&(inputXSecond>screenWight/2))||(((inputXSecond>0)&&(inputXSecond < screenWight / 2))&&(inputX>screenWight/2))) {
                clock.hideOff();
                if(airplane.acceptShot()) {
                    shots.add(airplane.createShot());
                }
                if (timeClock == 0) {
                    saveEngine.saveParam(saveEngine.getMusicValue(),saveEngine.getSoundValue(),saveEngine.getVibrateValue(),saveEngine.getLastPointValue(),saveEngine.getSaveAirplaneModelValue(),true,saveEngine.getControlVersionValue());
                    gsm.set(new SelectAirplaneState(gsm,new Background(realW,realH,false,scaleConstH,scaleConstW),saveEngine,realH,realW));
                    gsm.stopFly();
                } else {
                    timeClock--;
                    clock.nextClock();
                }
            } else {
                if(sense==senseMax){
                    clock.hideOff();
                    timeClock = 25;
                }
            }
            return;
        }
    }

    @Override
    public void update(float dt) {
        handleInput();

        for (int i = 0; i < shots.size(); i++) {
            if (shots.get(i).getShotLife() <= 0) {
                shots.remove(i);
                i--;
            }
        }

        background.updateBackground(dt);

        airplane.update(dt,new ArrayList<House>(),gsm);
        if(stepHelp==1)
            move.setPosition(airplane.getAirplane().getX()+move.getWidth()+20,airplane.getAirplane().getY()+5);
        if(stepHelp==2)
            move.setPosition(airplane.getAirplane().getX()+move.getWidth()+20,airplane.getAirplane().getY()-move.getHeight()+10);

        if(stepHelp<3) {
            if (blinkMove == 0) {
                moveFlag = !moveFlag;
                blinkMove = 20;
            } else {
                blinkMove--;
            }
        }

        for (Shot shot : shots){
            shot.update(dt,new ArrayList<House>());
        }

    }

    @Override
    public void render(SpriteBatch sb) {

        camera.update();

        sb.setProjectionMatrix(camera.combined);

        sb.begin();
        background.drawBackground(sb);
        clock.drawClock(sb);
        for (Shot shot : shots){
            shot.getShot().draw(sb);
        }
        airplane.getAirplane().draw(sb);

        text.draw(sb);
        if(moveFlag)
            move.draw(sb);
        if((stepHelp==1)||(stepHelp==3))
            handRight.draw(sb);
        if(stepHelp>1)
            handLeft.draw(sb);

        sb.end();
    }

    @Override
    public void dispose() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void resetting() {
        camera.setToOrtho(false,screenWight,screenHeight);
    }
}

