package com.phoenixshoi.airplane.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.phoenixshoi.airplane.element.ButtonGame;
import com.phoenixshoi.airplane.engine.SaveEngine;
import com.phoenixshoi.airplane.sprites.Background;
import com.phoenixshoi.airplane.sprites.Clock;

/**
 * Created by phoenixshoi on 24.06.16.
 */
public class MenuState extends State {
    private int screenWight;
    private int screenHeight;

    private SaveEngine save;

    ButtonGame playButton;
    ButtonGame settingsButton;
    //ButtonGame scoreButton;


    public MenuState(GameStateManager gsm,SaveEngine save,Background background,int stateHeight,int stateWidth){
        super(gsm,stateHeight,stateWidth);
        screenWight = stateWidth;
        screenHeight = stateHeight;


        this.background = background;

        startClass(save);

    }

    public MenuState(GameStateManager gsm,SaveEngine save,int stateHeight,int stateWidth){
        super(gsm,stateHeight,stateWidth);
        screenWight = stateWidth;
        screenHeight = stateHeight;


        background = new Background(screenWight,screenHeight,false,scaleConstH,scaleConstW);
        startClass(save);
    }

    private void startClass(SaveEngine save) {
        this.save=save;
        camera.setToOrtho(false,screenWight,screenHeight);
        resetting();
    }

    @Override
    protected void handleInput() {

        if(Gdx.input.justTouched()){
            int inputX=Gdx.input.getX();
            int inputY=screenHeight-Gdx.input.getY();

            if(playButton.checkClick(inputX,inputY)) {
                if(!save.getHelpSkipValue())
                    gsm.set(new HelpState(gsm,save,screenHeight,screenWight));
                else
                    gsm.set(new SelectAirplaneState(gsm,background,save,screenHeight,screenWight));
            }

            if(settingsButton.checkClick(inputX,inputY)) {
                gsm.set(new SettingState(gsm,save,this,background,screenHeight,screenWight));
            }
        }
        Gdx.input.setCatchBackKey(true);
    }

    @Override
    public void update(float dt) {
        background.logoOn();
        handleInput();
        background.updateBackground(dt);

    }

    @Override
    public void render(SpriteBatch sb) {

        camera.update();

        sb.setProjectionMatrix(camera.combined);

        sb.begin();
        background.drawBackground(sb);

        playButton.getSpriteButton().draw(sb);
        settingsButton.getSpriteButton().draw(sb);
        //scoreButton.getSpriteButton().draw(sb);
        sb.end();
    }

    @Override
    public void dispose() {

    }

    @Override
    public void resume() {
        background.resume();

    }

    @Override
    public void resetting() {
        playButton = new ButtonGame(screenWight/2,screenHeight/2,screenWight/2,screenHeight/2,"Buttons/playButton.png",save.getVibrateValue(),false,scaleConstH,scaleConstW);
        settingsButton = new ButtonGame(screenWight/2,screenHeight/2-(90*(Math.max(480,screenHeight)/Math.min(480,screenHeight))),screenWight/2,screenHeight/2-(90*(Math.max(480,screenHeight)/Math.min(480,screenHeight))),"Buttons/settingsButton.png",save.getVibrateValue(),false,scaleConstH,scaleConstW);
        //scoreButton = new ButtonGame(screenWight/2+90,screenHeight/2-90,screenWight/2+90,screenHeight/2-90,"Buttons/scoreButton.png",save.getVibrateValue(),false);
    }
}
