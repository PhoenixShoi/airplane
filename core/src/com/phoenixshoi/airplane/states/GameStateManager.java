package com.phoenixshoi.airplane.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.phoenixshoi.airplane.engine.SaveEngine;

import java.util.Stack;

/**
 * Created by phoenixshoi on 24.06.16.
 */
public class GameStateManager {

    private Stack<State> states;

    private static Music backgroundMusic;
    protected static Music flySound;
    protected static Sound shotSound;
    protected static Sound explosionSound;

    public  GameStateManager(){

        System.err.println(Gdx.files.isExternalStorageAvailable());
        System.err.println(Gdx.files.isLocalStorageAvailable());

        backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("Music/Background/backgroundMusic.ogg"));
        backgroundMusic.setLooping(true);
        shotSound = Gdx.audio.newSound(Gdx.files.internal("Music/Sound/shot.ogg"));
        flySound = Gdx.audio.newMusic(Gdx.files.internal("Music/Sound/fly.ogg"));
        flySound.setLooping(true);
        explosionSound = Gdx.audio.newSound(Gdx.files.internal("Music/Sound/explosion.ogg"));

        states = new Stack<State>();
    }

    public void setVolume(float value){
        System.err.println(value);
        if((value>1f)||(value<0))
            backgroundMusic.setVolume(0);
        else
            backgroundMusic.setVolume(value);
    }

    public void playFly(){
        if(!flySound.isPlaying())
            flySound.play();
    }

    public void stopFly(){
        if(flySound.isPlaying())
            flySound.stop();
    }

    public void playShot(){
        shotSound.setVolume(shotSound.play(),((new SaveEngine().getSoundValue())/100f));
    }

    public void playExplosion(){
        explosionSound.setVolume(explosionSound.play(),((new SaveEngine().getSoundValue())/100f));
    }

    public void setFlyVolume(float value){
        flySound.setVolume(value);
    }

    public void push(State state){

        states.push(state);
    }

    public void startMusic(){
        if(!backgroundMusic.isPlaying()){
            backgroundMusic.play();
            setVolume((new SaveEngine().getMusicValue())/100f);
            setFlyVolume((new SaveEngine().getSoundValue())/100f);
        }
    }

    public void pop(){
        states.pop().dispose();
    }

    public void set(State state){
        if(!backgroundMusic.isPlaying()){
            backgroundMusic.play();
            setVolume((new SaveEngine().getMusicValue())/100f);
            setFlyVolume((new SaveEngine().getSoundValue())/100f);
        }
        states.pop().dispose();
        states.push(state);
    }

    public void update(float dt){
            states.peek().update(dt);
    }

    public void render(SpriteBatch sb){
        states.peek().render(sb);
    }

    public void resume() {
        states.peek().resume();
    }

    public void resize() {
    }
}
