package com.phoenixshoi.airplane.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.phoenixshoi.airplane.element.ButtonGame;
import com.phoenixshoi.airplane.engine.SaveEngine;
import com.phoenixshoi.airplane.sprites.Background;

/**
 * Created by phoenixshoi on 20.07.16.
 */
public class PauseState extends State {

    private PlayState saveGame;
    private SaveEngine saveEngine;

    private ButtonGame resumeGameButton;
    private ButtonGame settingsButton;
    private ButtonGame musicButton;
    private ButtonGame vibrationButton;
    private ButtonGame exitButton;

    private int screenWight;
    private int screenHeight;

    private int backButtonTime=15;

    public PauseState(GameStateManager gsm, PlayState game, SaveEngine save,int stateHeight,int stateWidth){
        super(gsm,stateHeight,stateWidth);

        screenWight = stateWidth;
        screenHeight = stateHeight;


        saveGame = game;

        saveEngine = save;

        this.background=new Background(screenWight,screenHeight,false,scaleConstH,scaleConstW);;
        background.houseOff();

        resetting();


    }

    @Override
    protected void handleInput() {

        int inputX=0;
        int inputY=0;

        if(Gdx.input.justTouched()) {
            inputX = Gdx.input.getX();
            inputY = screenHeight - Gdx.input.getY();
        }
        if((!resumeGameButton.checkStatusButton())&&(!settingsButton.checkStatusButton())&&(!musicButton.checkStatusButton())&&(!vibrationButton.checkStatusButton())&&(!exitButton.checkStatusButton())){
            if(resumeGameButton.checkClick(inputX,inputY)){
                background.houseOn();
                saveGame.resetting();
                gsm.set(saveGame);
                return;
            }
            if(settingsButton.checkClick(inputX,inputY)){
                backButtonTime=15;
                gsm.set(new SettingState(gsm,saveEngine,this,background,screenHeight,screenWight));
                return;
            }
            if(musicButton.checkClick(inputX,inputY)){
                if((saveEngine.getSoundValue()>0)||(saveEngine.getMusicValue()>0)){
                    saveEngine.saveParam(0,0,saveEngine.getVibrateValue(),saveEngine.getLastPointValue(),saveEngine.getSaveAirplaneModelValue(),saveEngine.getHelpSkipValue(),saveEngine.getControlVersionValue());
                    gsm.setVolume(0);
                    gsm.setFlyVolume(0f);
                }
                else {
                    saveEngine.saveParam(100,100,saveEngine.getVibrateValue(),saveEngine.getLastPointValue(),saveEngine.getSaveAirplaneModelValue(),saveEngine.getHelpSkipValue(),saveEngine.getControlVersionValue());
                    gsm.setVolume(1f);
                    gsm.setFlyVolume(1f);
                }
                resetting();
                return;
            }
            if(vibrationButton.checkClick(inputX,inputY)){
                if(saveEngine.getVibrateValue()>0){
                    saveEngine.saveParam(saveEngine.getMusicValue(),saveEngine.getSoundValue(),0,saveEngine.getLastPointValue(),saveEngine.getSaveAirplaneModelValue(),saveEngine.getHelpSkipValue(),saveEngine.getControlVersionValue());
                }
                else {
                    saveEngine.saveParam(saveEngine.getMusicValue(),saveEngine.getSoundValue(),100,saveEngine.getLastPointValue(),saveEngine.getSaveAirplaneModelValue(),saveEngine.getHelpSkipValue(),saveEngine.getControlVersionValue());
                }
                resetting();
                return;
            }
            if(exitButton.checkClick(inputX,inputY)){
                gsm.push(new MenuState(gsm,saveEngine,background,screenHeight,screenWight));
                return;
            }
        }

        if((backButtonTime==0)&&(Gdx.input.isKeyPressed(Input.Keys.BACK))){
            backButtonTime=15;
            Gdx.input.setCatchBackKey(true);
            background.houseOn();
            saveGame.resetting();
            gsm.set(saveGame);
        }

        if(backButtonTime>0){
            backButtonTime--;
        }
    }

    @Override
    public void update(float dt) {
        handleInput();
        background.updateBackground(dt);
    }

    @Override
    public void render(SpriteBatch sb) {

        camera.update();

        sb.setProjectionMatrix(camera.combined);

        sb.begin();
        background.drawBackground(sb);
        resumeGameButton.getSpriteButton().draw(sb);
        settingsButton.getSpriteButton().draw(sb);
        musicButton.getSpriteButton().draw(sb);
        vibrationButton.getSpriteButton().draw(sb);
        exitButton.getSpriteButton().draw(sb);
        sb.end();
    }

    @Override
    public void dispose() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void resetting() {
        camera.setToOrtho(false,screenWight,screenHeight);
        exitButton = new ButtonGame(screenWight/2,screenHeight/2-90*scaleConstH,screenWight/2,screenHeight/2-90*scaleConstH,"Buttons/exitButton.png",saveEngine.getVibrateValue(),false,scaleConstH,scaleConstW);
        resumeGameButton = new ButtonGame(screenWight/2,screenHeight/2+90*scaleConstH,screenWight/2,screenHeight/2+90*scaleConstH,"Buttons/playButton.png",saveEngine.getVibrateValue(),false,scaleConstH,scaleConstW);
        settingsButton = new ButtonGame(screenWight/2,screenHeight/2,screenWight/2,screenHeight/2,"Buttons/settingsButton.png",saveEngine.getVibrateValue(),false,scaleConstH,scaleConstW);
        if((saveEngine.getMusicValue()>0)||(saveEngine.getSoundValue()>0))
            musicButton = new ButtonGame(screenWight/2-45*scaleConstW,screenHeight-45*scaleConstH,screenWight/2-45*scaleConstW,screenHeight-45*scaleConstH,"Buttons/volumeOn.png",saveEngine.getVibrateValue(),false,scaleConstH,scaleConstW);
        else
            musicButton = new ButtonGame(screenWight/2-45*scaleConstW,screenHeight-45*scaleConstH,screenWight/2-45*scaleConstW,screenHeight-45*scaleConstH,"Buttons/volumeOff.png",saveEngine.getVibrateValue(),false,scaleConstH,scaleConstW);
        if(saveEngine.getVibrateValue()>0)
            vibrationButton = new ButtonGame(screenWight/2+45*scaleConstW,screenHeight-45*scaleConstH,screenWight/2+45*scaleConstW,screenHeight-45*scaleConstH,"Buttons/vibrateOn.png",saveEngine.getVibrateValue(),false,scaleConstH,scaleConstW);
        else
            vibrationButton = new ButtonGame(screenWight/2+45*scaleConstW,screenHeight-45*scaleConstH,screenWight/2+45*scaleConstW,screenHeight-45*scaleConstH,"Buttons/vibrateOff.png",saveEngine.getVibrateValue(),false,scaleConstH,scaleConstW);
    }
}
