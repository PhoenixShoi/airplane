package com.phoenixshoi.airplane.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;

import org.lwjgl.Sys;

import java.util.ArrayList;

/**
 * Created by phoenixshoi on 12.07.16.
 */

public class Background {

    private Texture backstageTexture;
    private Texture downColorTexture;
    private Texture grassTexture;
    private Texture skyTexture;

    private Texture winTexture;

    private ArrayList<Texture> treeTextures;
    private ArrayList<Integer> treeX;


    private Texture upColorTexture;
    private Sprite tempSprite;

    private int screenWight;
    private int screenHeight;

    private int countGrass=2;
    private int countBackstage=2;
    private int countSky=2;

    private int countTree=2;
    private int countTreeMax=11;
    private int countTreeMin=8;
    private int countTreeLongGen=20;

    private ArrayList<Cloud> clouds;
    private ArrayList<House> houses;

    private boolean houseFlag=false;
    private boolean logoFlag=false;
    private boolean theEnd=false;

    private Sun sun;
    private Score score;

    private Texture logoTexture;
    private Sprite logo;

    public Background(int screenW,int screenH,boolean houseOn,int scaleH,int scaleW){
        screenWight=screenW;
        screenHeight=screenH;

        houseFlag = houseOn;

        treeTextures = new ArrayList<Texture>();
        treeX = new ArrayList<Integer>();

        clouds = new ArrayList<Cloud>();
        houses = new ArrayList<House>();


        this.loadTexture(MathUtils.random(1,countGrass),
                         MathUtils.random(1,countBackstage),
                         MathUtils.random(countTreeMin,countTreeMax),
                         MathUtils.random(1,countSky));

        score = new Score(screenW/2-19,screenH-screenH/5,10);
        logoTexture = new Texture("Backgrounds/Logo/logo.png");
        logo = new Sprite(logoTexture);
        logo.setSize((logo.getWidth()/2)*scaleW,(logo.getHeight()/2)*scaleH);
        logo.setPosition(screenW/2-logo.getWidth()/2,screenH-screenH/4-logo.getHeight()/2);
    }

    public void setScoreMax(int scoreMax){
        score = new Score(screenWight/2-13,screenHeight-screenHeight/5,scoreMax);
    }

    public ArrayList<House> getHouses(){
        return houses;
    }

    private void loadTexture(int grass,int backstage,int numTree,int sky){
        downColorTexture = new Texture("Backgrounds/DownColors/downColor"+grass+".png");
        grassTexture = new Texture("Backgrounds/GrassModels/grassModel"+grass+".png");
        backstageTexture = new Texture("Backgrounds/BackstageModels/backstageModel"+backstage+".png");
        skyTexture = new Texture("Backgrounds/SkyModels/skyModel"+sky+".png");
        upColorTexture = new Texture("Backgrounds/UpColors/upColor"+sky+".png");



        for(int i=0;i<numTree;i++){
            treeTextures.add(new Texture("Backgrounds/TreeModels/treeModel"+MathUtils.random(1,countTree)+".png"));
            int newPoint=MathUtils.random(0,screenWight);
            for(int j=0;j<treeX.size();j++){
                if((newPoint>treeX.get(j)-countTreeLongGen)&&(newPoint<treeX.get(j)+countTreeLongGen)){
                    newPoint=MathUtils.random(0,screenWight);
                    j=0;
                }
            }

            treeX.add(newPoint);
        }

        if(houseFlag) {

            houses.add(new House(screenWight / 8, getHeightAirplane(), 1, false));
            houses.add(new House(screenWight - screenWight / 8, getHeightAirplane(), 2, true));
        }

        boolean tmpCloudFlag=MathUtils.randomBoolean();

        for (int i=1;i<=10;i++)
        {
            clouds.add(new Cloud(MathUtils.random(0,screenWight),MathUtils.random(screenHeight/2+50,screenHeight-20),tmpCloudFlag,screenHeight,screenWight));
        }

        sun = new Sun(MathUtils.random(0,screenWight),MathUtils.random(screenHeight/2+70,screenHeight-70),screenHeight,screenWight);
    }

    public int getHeightAirplane(){
        return (int) (screenHeight*0.1+grassTexture.getHeight()*0.5f)-16;
    }


    public void drawBackground(SpriteBatch sb){

        for(int i = screenHeight;i>(int) ((screenHeight*0.1)+40+backstageTexture.getHeight())/2;i=i-upColorTexture.getHeight()){
            textureDraw(upColorTexture,sb,0,i);
        }

        textureDraw(skyTexture,sb,0,(int) ((screenHeight*0.1)+40+backstageTexture.getHeight())/2,true,0.5f);

        textureDraw(backstageTexture,sb,0,(int) (screenHeight*0.1)+40,true,0.5f);

        for(int i=0;i<treeTextures.size();i++){
            textureDraw(treeTextures.get(i),sb,treeX.get(i),(int) (screenHeight*0.1)+40,0.5f,0.5f);
        }

        for(int i = (int) (screenHeight*0.1);i+downColorTexture.getHeight()>0;i=i-downColorTexture.getHeight()){
            textureDraw(downColorTexture,sb,0,i);
        }


        textureDraw(grassTexture,sb,0,(int) (screenHeight*0.1),true,0.5f);

        sun.getSun().draw(sb);



        for (Cloud cloud : clouds){
            cloud.getCloud().draw(sb);
        }

        if(houseFlag)
            for (House house : houses){
                house.getHouse().draw(sb);
                house.getDuctSmoke().draw(sb);
                house.getDuct().draw(sb);
            }

        if(houseFlag) {
            if(theEnd){
                textureDraw(winTexture,sb,screenWight/2-winTexture.getWidth()/4,screenHeight-screenHeight/4-winTexture.getHeight()/4,0.5f,0.5f);
            }
            else
                score.update(sb);
        }
        if(logoFlag)
            logo.draw(sb);
    }

    public void updateBackground(float dt){
        for (Cloud cloud : clouds){
            cloud.update(dt);
        }

        sun.update(dt);
    }



    private void textureDraw(Texture tmp,SpriteBatch sb,int x,int y){
        this.textureDraw(tmp,sb,x,y,false,false,false,0,0);
    }

    private void textureDraw(Texture tmp,SpriteBatch sb,int x, int y,boolean switchWH,float scale){

        if(switchWH)
            this.textureDraw(tmp,sb,x,y,true,false,false,0,scale);
        else
            this.textureDraw(tmp,sb,x,y,false,true,false,scale,0);
    }

    private void textureDraw(Texture tmp,SpriteBatch sb,int x, int y,float scaleW,float scaleH){
        this.textureDraw(tmp,sb,x,y,false,false,true,scaleW,scaleH);
    }

    private void textureDraw(Texture tmp,SpriteBatch sb,int x,int y,boolean resizeW,boolean resizeH,boolean resizeWH,float scaleW,float scaleH){
        tempSprite = new Sprite(tmp);

        if(resizeW)
            tempSprite.setSize(screenWight,tmp.getHeight()*scaleH);
        if(resizeH)
            tempSprite.setSize(tmp.getWidth()*scaleW,screenHeight);
        if(resizeWH)
            tempSprite.setSize(tmp.getWidth()*scaleW,tmp.getHeight()*scaleH);
        tempSprite.setPosition(x,y);
        tempSprite.draw(sb);
    }

    public Score getScore(){
        return score;
    }

    public void resume() {

        for (Cloud cloud : clouds){
            cloud.resume();
        }

    }

    public void checkWin(int winNum,int colorIDFirstPlayer,int colorIDSecondPlayer) {
        if(winNum>0) {
            theEnd=true;
            if(winNum==3){
                winTexture= new Texture("PlayerWinModels/playersDraw.png");
                return;
            }
            if(winNum==1){
                winTexture= new Texture("PlayerWinModels/playerWin1"+colorIDFirstPlayer+".png");
                return;
            }
            else {
                winTexture= new Texture("PlayerWinModels/playerWin2"+colorIDSecondPlayer+".png");
            }

        }

    }

    public boolean getTheEnd(){
        return theEnd;
    }

    public void logoOff() {
        logoFlag=false;
    }

    public void logoOn(){
        logoFlag=true;
    }

    public void houseOff() {
        houseFlag=false;
    }

    public void houseOn() {
        houseFlag=true;
    }
}
