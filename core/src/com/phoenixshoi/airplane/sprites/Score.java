package com.phoenixshoi.airplane.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.ArrayList;

/**
 * Created by phoenixshoi on 14.07.16.
 */
public class Score {

    private int scoreMax=0;

    private Numbers scoreLeft;
    private Numbers scoreRight;

    private int time=50;

    private int space=26;

    public Score(int x,int y,int scoreMax){
        this.scoreMax=scoreMax;

        scoreLeft = new Numbers(x,y,-space,0,false);
        scoreRight = new Numbers(x,y,space,0,false);

    }

    public void update(SpriteBatch sb) {
        if (time > 0) {

            scoreLeft.update(sb);
            scoreRight.update(sb);

            time--;
        }
    }

    public void scoreLeftUp(){

        time=50;
        scoreLeft.setValue(scoreLeft.getValue()+1);
    }

    public void scoreRightUp(){

        time=50;
        scoreRight.setValue(scoreRight.getValue()+1);
    }


    public void scoreLeftDown() {

        time=50;
        scoreLeft.setValue(scoreLeft.getValue()-1);
    }

    public void scoreRightDown(){

        time=50;
        scoreRight.setValue(scoreRight.getValue()-1);
    }

    public int checkWin() {
        int scoreL=scoreLeft.getValue();
        int scoreR=scoreRight.getValue();

        if((scoreL==scoreMax)&&(scoreR==scoreMax)){
            return 3;
        }
        if(scoreL==scoreMax){
            return 1;
        }
        if(scoreR==scoreMax){
            return 2;
        }
        return 0;
    }
}
