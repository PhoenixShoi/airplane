package com.phoenixshoi.airplane.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.phoenixshoi.airplane.states.GameStateManager;


import java.util.ArrayList;

/**
 * Created by phoenixshoi on 24.06.16.
 */

public class Airplane {

    private Vector3 velosity;
    private int startPointX;
    private int startPointY;
    private int firstVelosityX;
    private int firstVelosityY;

    private Circle circleAirplane;

    private Texture airplaneTexture;
    private Sprite airplane;

    private static final int FRAME_COLS = 8;
    private static final int FRAME_ROWS = 2;
    private Texture explosionTexture;

    private Boolean airplaneDead=false;

    private int explosionIndexRow=0;
    private int explosionIndexCol=0;


    private static float countMax=3;
    private static float speed=60;

    private float countX=0;
    private float countY=0;

    private float startY=3;

    private boolean engineFlag=false;

    private boolean flipFlag;

    private int ang=0;

    private int shotTime=20;
    private int shotTimeMax=20;


    private int countAirplaneModels = 4;


    private int airplaneId=0;

    private float screenHeight;
    private float screenWidth;

    private static int deathHeight;

    private int colorID;
    private int vibrateValue;
    private int soundValue;

    //private Texture arT;
    //private Sprite ar;

    public Airplane(int x,int y,boolean flipF,int numberModelAirplane,int id,int vibrateValue,int soundValue,int screenHeight,int screenWidth,int deathHeight){

        this.deathHeight=deathHeight-3;

        //arT= new Texture("ar.png");
        //ar = new Sprite(arT);

        this.vibrateValue=vibrateValue;

        this.screenHeight=screenHeight;
        this.screenWidth=screenWidth;

        airplaneId=id;


        if((numberModelAirplane<0)||(numberModelAirplane>countAirplaneModels))
            numberModelAirplane=0;


        if(!flipF)
            startY=-3;


        colorID=numberModelAirplane;

        airplaneTexture = new Texture("AirplaneModels/airplaneModel"+numberModelAirplane+".png");
        airplane = new Sprite(airplaneTexture);

        flipFlag=flipF;

        explosionTexture = new Texture(Gdx.files.internal("OtherModels/ExplosionPack/airplaneExplosion"+numberModelAirplane+".png"));

        startPointX = x;
        startPointY = y;

        resume();

        circleAirplane = new Circle(x,y,airplane.getWidth()/2);
        airplane.flip(flipFlag,false);
        resetPosition(x,y);

    }

    public void startEngine(){
        countY=startY;
        engineFlag=true;
        shotTime+=20;
    }

    public Sprite getAirplane() {
        return airplane;
    }

    public Circle getCircleAirplane(){ return circleAirplane;}

    public Shot createShot(){
        shotTime=shotTimeMax;
        return new Shot(airplane.getX()+countY*10, airplane.getY()+countX*10, countX, countY, ang,startPointY,airplaneId,screenHeight,screenWidth);
    }

    public boolean acceptShot(){
        if((airplaneDead)||(!engineFlag))
            return false;

        if(shotTime==0)
            return true;
        else
            return false;
    }


    public void setLeft(){

        if(airplaneDead)
            return;

        if(!engineFlag) {
            startEngine();
            return;
        }

        ang%=360;

        ang+=15;

        if((countY==countMax)&&(countX<=countMax-1)){
            countX++;
            return;
        }
        if((countX==countMax)&&(countY>=-countMax+1)){
            countY--;
            return;
        }
        if((countY==-countMax)&&(countX>=-countMax+1)){
            countX--;
            return;
        }
        if((countX==-countMax)&&(countY<=countMax-1)){
            countY++;
            return;
        }
    }

    public void setRight(){

        if(airplaneDead)
            return;

        if(!engineFlag) {
            startEngine();
            return;
        }

        ang%=360;

        ang-=15;
        if((countY==-countMax)&&(countX<=countMax-1)){
            countX++;
            return;
        }
        if((countX==countMax)&&(countY<=countMax-1)){
            countY++;
            return;
        }
        if((countY==countMax)&&(countX>=-countMax+1)){
            countX--;
            return;
        }
        if((countX==-countMax)&&(countY>=-countMax+1)){
            countY--;
            return;
        }

    }

    public void aiControl(Airplane player, ArrayList<Shot> shots,GameStateManager gsm){

        float strikeX;
        float strikeY;

        if(airplane.getY()<startPointY+50){
            setRight();
            return;
        }

        if(MathUtils.random(1,1000)>300){
            strikeX=MathUtils.random(1,screenWidth);
            strikeY=screenHeight/2;
        }
        else {
            strikeX=player.getAirplane().getX();
            strikeY=player.getAirplane().getY();
        }

        if(!player.getAirplaneDead()){
            float a=Math.abs(airplane.getX()-strikeX);
            float b=Math.abs(airplane.getY()-strikeY);
            float c= (float) (Math.atan2(b,a)*(180/Math.PI));
            if ((player.getAirplane().getY()>airplane.getY())&&(player.getAirplane().getX()<airplane.getX())){
                c=180-c;
            }
            if ((player.getAirplane().getY()<airplane.getY())&&(player.getAirplane().getX()<airplane.getX())){
                c+=180;
            }
            if ((player.getAirplane().getY()<airplane.getY())&&(player.getAirplane().getX()>airplane.getX())){
                c=360-c;
            }
            //ar.setRotation(c);
            int preAng=ang;
            if(preAng>=0){
                if(preAng<180){
                    preAng+=180;
                }
                else {
                    preAng-=360;
                }
            }
            else {
                if(preAng>-180){
                    preAng=180-Math.abs(preAng);
                }
                else {
                    preAng=360+preAng;
                }
            }
            if((preAng>=c-5)&&(preAng<=c+5)){
                if (acceptShot()) {
                    gsm.playShot();
                    shots.add(createShot());
                    return;

                }
            }
            if(preAng>c){
                setRight();
                return;
            }
            else {
                setLeft();
                return;
            }
            //System.err.println(airplane.getX()+" "+airplane.getY());
        }

    }

    public int getColorID(){
        return colorID;
    }

    public void update(float dt, ArrayList<House> houses, GameStateManager gsm){


        if((shotTime>0)&&(engineFlag))
            shotTime--;

        if(airplane.getY()<deathHeight)
            airplaneDead=true;

        for(House house : houses){
            if(house.checkDamage(circleAirplane)) {
                airplaneDead=true;
                break;
            }
        }

        if(airplaneDead){

            engineFlag=false;
            countY=0;

            float lastX=airplane.getX();
            float lastY=airplane.getY();

            airplane = new Sprite(explosionTexture,explosionIndexCol*24,explosionIndexRow*24,24,24);
            airplane.setPosition(lastX,lastY);
            if((explosionIndexRow==0)&&(explosionIndexCol==0)) {
                gsm.playExplosion();
                gsm.stopFly();
            }

            explosionIndexCol++;
            if(explosionIndexCol>=FRAME_COLS){
                explosionIndexCol=0;
                explosionIndexRow++;
            }
            if(explosionIndexRow>=FRAME_ROWS){
                explosionIndexRow=0;
                explosionIndexCol=0;
                countX=firstVelosityX;
                countY=firstVelosityY;
                velosity.set(0,0,0);
                airplaneDead=false;
                ang=0;
                airplane = new Sprite(airplaneTexture);
                airplane.flip(flipFlag,false);
                resetPosition(startPointX,startPointY);
            }
        }
        else {

            if(engineFlag)
                gsm.playFly();


            velosity.scl(dt);

            velosity.add(countY * speed, countX * speed, 0);


            velosity.scl(dt);

            resetPosition(airplane.getX()+velosity.x, airplane.getY()+velosity.y);

            if (airplane.getX() < 0)
                resetPosition(screenWidth,airplane.getY());

            if (airplane.getY() < 0)
                resetPosition(airplane.getX(),screenHeight);

            if (airplane.getX() > screenWidth)
                resetPosition(0,airplane.getY());

            if (airplane.getY() > screenHeight-5)
                resetPosition(airplane.getX(),screenHeight-5);

            velosity.scl(1 / dt);

        }

        airplane.setRotation(ang);
    }

    private void resetPosition(float x,float y){
        airplane.setPosition(x, y);
        //ar.setPosition(x,y);
        circleAirplane.setPosition(x,y);
    }

    /*public Sprite getAr(){
        return ar;
    }*/

    public void setAirplaneDead(){
        Gdx.input.vibrate(vibrateValue);
        airplaneDead=true;
    }

    public boolean getAirplaneDead(){
        return airplaneDead;
    }

    public int getId() {
        return airplaneId;
    }

    public void resume(){
        velosity = new Vector3(0,0,0);
    }

}
