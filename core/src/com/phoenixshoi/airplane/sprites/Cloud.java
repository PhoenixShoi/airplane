package com.phoenixshoi.airplane.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by phoenixshoi on 10.07.16.
 */
public class Cloud {
    private Vector3 velosity;

    private Texture cloudTexture;
    private Sprite cloud;

    private float speed=1;
    private float speedMax=5;

    private float countY=3;

    private static int screenHeight;
    private static int screenWidth;


    private static int selectCloudPack = MathUtils.random(1,2);

    public Cloud(float x,float y,boolean flagCloudFlip,int screenHeight,int screenWidth){

        this.screenHeight=screenHeight;
        this.screenWidth=screenWidth;

        if(flagCloudFlip)
            countY=-3;

        speed=MathUtils.random(1,speedMax);

        resume();
        cloudTexture = new Texture("CloudModels/CloudPack"+selectCloudPack+"/cloud"+MathUtils.random(1,selectCloudPack+1)+".png");
        cloud = new Sprite(cloudTexture);
        cloud.setSize(cloud.getWidth()/4,cloud.getHeight()/4);
        cloud.setPosition(x,y);

    }

    public Sprite getCloud() {
        return cloud;
    }


    public void update(float dt){


        velosity.scl(0);

        velosity.add(countY*speed,0, 0);

        velosity.scl(dt);

        cloud.setPosition(cloud.getX()+velosity.x,cloud.getY()+velosity.y);

        if(cloud.getX()+cloud.getWidth()<0) {
            cloud.setX(screenWidth+cloud.getWidth());
            speed = MathUtils.random(1, speedMax);
        }

        if(cloud.getX()-cloud.getWidth()>screenWidth) {
            cloud.setX(-cloud.getWidth());
            speed=MathUtils.random(1,speedMax);
        }

        velosity.scl(1/dt);

    }

    public void resume() {
        velosity = new Vector3(0,0,0);
    }
}
