package com.phoenixshoi.airplane.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Shape2D;

/**
 * Created by phoenixshoi on 11.07.16.
 */

public class House {

    private Texture houseTexture;
    private Sprite house;

    private Texture ductTexture;
    private Sprite duct;

    private Texture ductSmokeTexture;
    private Sprite ductSmoke;

    private static final int FRAME_COLS = 3;
    private static final int FRAME_ROWS = 2;
    private int ductSmokeIndexRow=0;
    private int ductSmokeIndexCol=0;
    private int ductSmokeSpeed=0;
    private int ductSmokeSpeedMax=10;


    private int countHouse=2;

    private Circle rectangleHouse;

    public House(int x,int y,int modelNumber,boolean flip){

        if((modelNumber>countHouse)||(modelNumber<1))
            modelNumber=1;

        houseTexture = new Texture("HouseModels/houseModel"+modelNumber+".png");
        house = new Sprite(houseTexture);
        house.setPosition(x-this.getHouse().getWidth()/2,y);

        ductSmokeTexture = new Texture("HouseModels/ductSmoke.png");
        ductSmoke = new Sprite(ductSmokeTexture);

        ductTexture = new Texture("HouseModels/duct.png");
        duct = new Sprite(ductTexture);
        duct.setPosition(house.getX()+house.getWidth()/2+duct.getWidth(),house.getY()+house.getHeight()/2+duct.getHeight());


        house.flip(flip,false);
        duct.flip(flip,false);

        rectangleHouse = new Circle(house.getX(),house.getY(),25);


    }

    public Sprite getHouse() {
        return house;
    }

    public Sprite getDuct() {
        return duct;
    }

    public Sprite getDuctSmoke() {
        if(ductSmokeSpeed==0) {
            ductSmoke = new Sprite(ductSmokeTexture, ductSmokeIndexCol * 8, ductSmokeIndexRow * 10, 8, 10);
            ductSmoke.setPosition(duct.getX(), duct.getY() + 7);
            ductSmokeIndexCol++;
            if (ductSmokeIndexCol >= FRAME_COLS) {
                ductSmokeIndexCol = 0;
                ductSmokeIndexRow++;
            }
            if (ductSmokeIndexRow >= FRAME_ROWS) {
                ductSmokeIndexRow = 0;
                ductSmokeIndexCol = 0;
            }
            ductSmokeSpeed = ductSmokeSpeedMax;
        }
        else
            ductSmokeSpeed--;
        return ductSmoke;
    }

    public boolean checkDamage(Circle circle){
        if(rectangleHouse.overlaps(circle))
            return true;
        else
            return false;
    }



}
