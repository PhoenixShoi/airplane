package com.phoenixshoi.airplane.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;


/**
 * Created by phoenixshoi on 10.07.16.
 */
public class Shot {
    private Vector3 position;
    private Vector3 velosity;

    private Circle circleShot;

    private float speed=200;
    private int shotLife=60;

    private Texture shotTexture;
    private Sprite shot;

    private float countX;
    private float countY;

    private int heightMin;

    private int idParent=0;


    private static float screenHeight;
    private static float screenWidth;


    public Shot(float xPosition, float yPosition, float xVelosity, float yVelosity, int ang, int heightMinimal, int idAirplane, float screenHeight, float screenWidth){

        this.screenHeight=screenHeight;
        this.screenWidth=screenWidth;

        idParent=idAirplane;
        position = new Vector3(xPosition,yPosition,0);
        velosity = new Vector3(0,0,0);
        countX=xVelosity;
        countY=yVelosity;
        shotTexture = new Texture("OtherModels/shot.png");
        shot = new Sprite(shotTexture);

        shot.setRotation(ang);

        circleShot = new Circle(xPosition,yPosition,shot.getWidth());
        shot.scale(2);

        heightMin=heightMinimal;

    }

    public Sprite getShot() {
        return shot;
    }
    public int getShotLife() { return shotLife;}
    public Circle getCircleShot(){ return circleShot;}


    public void update(float dt, ArrayList<House> houses){

        if((position.y<heightMin-3)||(position.y>screenHeight)||(shotLife==0)){
            shotLife=0;
            return;
        }

        for(House house : houses){
            if(house.checkDamage(circleShot)){
                shotLife=0;
                return;
            }
        }

        velosity.scl(0);

        velosity.add(countY*speed,countX*speed, 0);

        velosity.scl(dt);

        position.add(velosity.x,velosity.y,0);

        if(position.x<0)
            position.x=screenWidth;

        if(position.x>screenWidth)
            position.x=0;

        shotLife--;

        velosity.scl(1/dt);

        shot.setPosition(position.x,position.y);
        circleShot.setPosition(position.x,position.y);

    }

    public int getIdParent(){
        return idParent;
    }

    public void resume() {
        position = new Vector3(shot.getX(),shot.getY(),0);
        velosity = new Vector3(0,0,0);
    }
}
