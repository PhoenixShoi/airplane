package com.phoenixshoi.airplane.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by phoenixshoi on 11.07.16.
 */
public class Sun {

    private Texture sunTexture;
    private Sprite sun;


    private static int screenHeight;
    private static int screenWidth;

    public Sun(int x,int y,int screenHeight,int screenWidth){

        this.screenHeight=screenHeight;
        this.screenWidth=screenWidth;

        sunTexture = new Texture("OtherModels/sun.png");
        sun = new Sprite(sunTexture);
        sun.setPosition(x,y);

    }

    public Sprite getSun() {
        return sun;
    }


    public void update(float dt){


        if(sun.getX()+sun.getWidth()<0) {
            sun.setX(screenWidth/2);
        }

        if(sun.getX()-sun.getWidth()>screenWidth) {
            sun.setX(screenWidth/2);
        }

    }
}
