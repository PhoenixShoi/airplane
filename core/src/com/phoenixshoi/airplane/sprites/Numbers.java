package com.phoenixshoi.airplane.sprites;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.ArrayList;

/**
 * Created by phoenixshoi on 21.07.16.
 */
public class Numbers {

    private int value=0;
    private int space;

    ArrayList<Number> numberz;

    private int startX;
    private int startY;
    private int newX;
    private int newY;

    private boolean magicFlag=false;

    public Numbers(int startX,int startY,int space,int startValue,boolean magicFlag){

        this.magicFlag=magicFlag;

        this.startX=startX;
        this.startY=startY;

        this.space=space;

        numberz = new ArrayList<Number>();

        setValue(startValue);

    }


    private int magic(){
        int sizeValue=0;

        newX=startX;
        newY=startY;


        while(value>=0){
            sizeValue++;
            value/=10;
            if(value==0)
                break;
        }

        if((magicFlag)&&(sizeValue!=numberz.size())){
            if(space<0){
                newX-=space*sizeValue/2;
            }
            else {
                newX+=space*sizeValue/2;
            }
            numberz = new ArrayList<Number>();
        }

        return sizeValue;
    }

    public void setValue(int newValue){
        if(newValue<0) {
            return;
        }

        value=newValue;

        int sizeValue=magic();

        if(sizeValue>numberz.size()){
            while(numberz.size()<sizeValue){
                if(numberz.size()==0)
                    numberz.add(new Number(newX+space,newY,0));
                else
                    numberz.add(new Number( (int) (numberz.get(numberz.size()-1).getNumber().getX()+space),newY,0));
            }
        }

        if(sizeValue<numberz.size()){
            while (numberz.size()>sizeValue){
                numberz.remove(numberz.size()-1);
            }
        }

        value=newValue;

        if(space>0) {
            for (int i = numberz.size()-1; i > -1; i--) {
                numberz.get(i).setMem(value % 10);
                value /= 10;
            }
        }
        else {
            for(Number number : numberz){
                number.setMem(value % 10);
                value /= 10;
            }
        }

        value=newValue;

    }

    public int getValue(){
        return value;
    }

    public void update(SpriteBatch sb){
        for(Number number : numberz){
            number.getNumber().draw(sb);
        }
    }
}
