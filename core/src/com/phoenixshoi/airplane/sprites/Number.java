package com.phoenixshoi.airplane.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Created by phoenixshoi on 14.07.16.
 */
public class Number {
    private Texture numberTexture;
    private Sprite number;


    private int startX;
    private int startY;

    private int speed=20;
    private int mem=0;


    public Number(int x,int y,int num){
        startX=x;
        startY=y;
        setMem(num);
    }

    private void update(){
        numberTexture = new Texture("OtherModels/NumbersPack/numberModel"+mem+".png");
        number = new Sprite(numberTexture);
        number.setSize(number.getWidth()/6,number.getHeight()/6);
        number.setPosition(startX,startY);
    }

    public Sprite getNumber(){
        return number;
    }

    public int getMem(){
        return mem;
    }

    public void setMem(int newMem){
        mem=newMem;
        update();
    }
}
