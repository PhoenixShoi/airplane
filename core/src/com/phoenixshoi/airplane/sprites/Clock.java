package com.phoenixshoi.airplane.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;

/**
 * Created by phoenixshoi on 26.07.16.
 */
public class Clock {

    private Texture clockTexture;
    private Sprite clock;

    private int originX;
    private int originY;

    private int num=4;

    private boolean hideFlag=false;

    private int timeNext=0;

    public Clock(int x,int y){

        originX=x;
        originY=y;

        nextClock();
    }

    public void hideOn(){
        hideFlag=true;
    }

    public void hideOff(){
        hideFlag=false;
    }

    public void drawClock(SpriteBatch sb){
        if(!hideFlag)
            clock.draw(sb);
    }

    public void nextClock(){
        if(timeNext==0){
            timeNext=5;
        }
        else {
            timeNext--;
            return;
        }
        num++;

        if(num>4){
            num=1;
        }

        clockTexture= new Texture("HelpModel/clock"+num+".png");
        clock = new Sprite(clockTexture);

        clock.setPosition(originX-clock.getHeight()/2,originY-clock.getWidth()/2);
    }

    public  int getNum(){
        return num;
    }
}
