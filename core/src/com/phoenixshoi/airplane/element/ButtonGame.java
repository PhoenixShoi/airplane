package com.phoenixshoi.airplane.element;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;

/**
 * Created by phoenixshoi on 11.07.16.
 */
public class ButtonGame {

    private Texture buttonTexture;
    private Sprite button;
    private static int vibrateButton;

    private boolean statusButton=false;

    private int buttonTime=0;

    private int realX;
    private int realY;

    public Sprite getSpriteButton(){
        return button;
    }

    public ButtonGame(int positionX,int positionY,int realCheckScreenX,int realCheckScreenY,String path,int vibrate,boolean flipFlag,int scaleH,int screenW){
        buttonTexture = new Texture(path);
        button = new Sprite(buttonTexture);

        button.setSize(button.getWidth()*screenW,button.getHeight()*scaleH);


        button.flip(flipFlag,false);
        realX= (int) (realCheckScreenX-button.getWidth()/2);
        realY= (int) (realCheckScreenY-button.getHeight()/2);
        button.setPosition(positionX-button.getWidth()/2,positionY-button.getHeight()/2);
        vibrateButton=vibrate;
    }

    public boolean checkClick(int x, int y){
        System.err.println("I button "+realX+" "+realY);
        if((x>=realX)&&((x<=realX+button.getWidth()))&&(buttonTime==0)&&((y<=realY+button.getHeight()))&&((y>=realY))) {
            Gdx.input.vibrate(vibrateButton*2);
            statusButton=true;
            buttonTime=15;
            return true;
        }
        else {
            if(buttonTime>0)
                buttonTime--;
            statusButton=false;
            return false;
        }
    }

    public boolean checkStatusButton(){
        return statusButton;
    }
}
