package com.phoenixshoi.airplane.element;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.phoenixshoi.airplane.sprites.Airplane;

/**
 * Created by phoenixshoi on 22.08.16.
 */
public class StickGame {

    Texture stickBackgroundTexture;
    Texture stickTexture;

    Sprite stickBackground;
    Sprite stick;


    public StickGame(){
        stickBackgroundTexture = new Texture(Gdx.files.internal(""));
        stickTexture = new Texture(Gdx.files.internal(""));

        stickBackground = new Sprite(stickBackgroundTexture);
        stick = new Sprite(stickTexture);
    }

    public void input(Airplane airplane){

    }

    public void draw(SpriteBatch sb){

    }
}
