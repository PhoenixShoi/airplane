package com.phoenixshoi.airplane.element;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Cursor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.phoenixshoi.airplane.sprites.Number;
import com.phoenixshoi.airplane.sprites.Numbers;

/**
 * Created by phoenixshoi on 21.07.16.
 */
public class ControlGame {

    private Numbers value;

    private Texture pointTexture;
    private Texture lineTexture;
    private Texture logoTexture;

    private Sprite point;
    private Sprite line;
    private Sprite logo;

    private boolean touchFlag=false;

    private int min;

    private static int screenHeight;
    private static int screenWidth;

    public ControlGame(int y,String path,int min,int screenHeight,int screenWidth,int scaleH,int scaleW){
        this.min=min;


        pointTexture = new Texture("Buttons/Control/pointControlModel.png");
        lineTexture = new Texture("Buttons/Control/lineModel.png");
        logoTexture = new Texture(path);

        point = new Sprite(pointTexture);
        line = new Sprite(lineTexture);
        logo = new Sprite(logoTexture);

        point.setSize(point.getHeight()/2,point.getWidth()/2);

        line.setSize(screenWidth-screenWidth/3,10);
        value = new Numbers(screenWidth/2,y,-25,0,true);

        line.setPosition((screenWidth/3)/2,y-point.getHeight()/2-5);

        point.setPosition((screenWidth/3)/2-point.getWidth()/2,line.getY()-point.getHeight()/2+line.getHeight()/2);

        logo.setSize((logo.getWidth()/4)*scaleW,(logo.getHeight()/4)*scaleH);
        logo.setPosition(screenWidth/2-logo.getWidth()/2,y+point.getHeight());

        this.screenHeight=screenHeight;
        this.screenWidth=screenWidth;

    }

    public void draw(SpriteBatch sb){
        line.draw(sb);
        point.draw(sb);
        value.update(sb);
        logo.draw(sb);
    }

    public void input(int x,int y){

        if(((x>=point.getX())&&((x<=point.getX()+point.getWidth()))&&((y<=point.getY()+point.getHeight()))&&((y>=point.getY())))||(touchFlag)||
                ((x>=line.getX())&&((x<=line.getX()+line.getWidth()))&&((y<=line.getY()+line.getHeight()+line.getHeight()))&&((y>=line.getY()-line.getHeight())))){
            point.setPosition(x-point.getWidth()/2,point.getY());
            touchFlag=true;
            if(point.getX()<(screenWidth/3)/2-point.getWidth()/2){
                point.setX((screenWidth/3)/2-point.getWidth()/2);
            }
            if(point.getX()>line.getX()+line.getWidth()-point.getWidth()/2){
                point.setX(line.getX()+line.getWidth()-point.getWidth()/2);
            }
            value.setValue((int) math());

        }

    }

    public void setOff(){
        touchFlag=false;
    }

    public int math(){

        int answer=(int) ((point.getX()+point.getWidth()/2-line.getX())/(line.getWidth()/100));

        if(answer<min){
            return min;
        }
        else {
            return answer;
        }

    }

    public void setValue(int newValue){
        if((newValue>-1)&&(newValue<101)){
            value.setValue(newValue);
            point.setX((line.getWidth()/100)*newValue-point.getWidth()/2+line.getX());
        }
    }
    public boolean getTouchFlag(){
        return touchFlag;
    }
}
