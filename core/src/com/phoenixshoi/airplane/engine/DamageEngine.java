package com.phoenixshoi.airplane.engine;

import com.phoenixshoi.airplane.sprites.Airplane;
import com.phoenixshoi.airplane.sprites.Score;
import com.phoenixshoi.airplane.sprites.Shot;

import java.util.ArrayList;

/**
 * Created by phoenixshoi on 16.07.16.
 */
public class DamageEngine {

    public DamageEngine(){
    }

    public int checkDamage(ArrayList<Shot> shots, ArrayList<Airplane> airplanes, Score score){
        for(int i=0;i<shots.size();i++){
            for(Airplane airplane : airplanes){
                if(!airplane.getAirplaneDead()){
                    if(shots.get(i).getCircleShot().overlaps(airplane.getCircleAirplane())){
                        if(shots.get(i).getIdParent()!=airplane.getId()){
                            if(airplane.getId()==0){
                                score.scoreRightUp();
                            }
                            else {
                                score.scoreLeftUp();
                            }
                        }
                        airplane.setAirplaneDead();
                        shots.remove(i);
                        i--;
                        break;
                    }
                }
            }
        }

        if((!airplanes.get(0).getAirplaneDead())&&(!airplanes.get(1).getAirplaneDead())){
            if(airplanes.get(0).getCircleAirplane().overlaps(airplanes.get(1).getCircleAirplane())){
                airplanes.get(0).setAirplaneDead();
                airplanes.get(1).setAirplaneDead();
                score.scoreLeftDown();
                score.scoreRightDown();
            }
        }

        return score.checkWin();
    }
}
