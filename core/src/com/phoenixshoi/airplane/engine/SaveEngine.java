package com.phoenixshoi.airplane.engine;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

/**
 * Created by phoenixshoi on 21.07.16.
 */
public class SaveEngine {
    private Preferences save;

    private static int musicValue;
    private static int soundValue;
    private static int vibrateValue;
    private static int lastPointValue;
    private static int saveAirplaneModelValue;
    private static boolean helpSkipValue;
    private static int controlVersionValue;

    public SaveEngine(){
        save = Gdx.app.getPreferences("Setting");
        musicValue=save.getInteger("MusicValue",100);
        soundValue=save.getInteger("SoundValue",100);
        vibrateValue=save.getInteger("VibrateValue",100);
        lastPointValue=save.getInteger("LastPointValue",50);
        saveAirplaneModelValue=save.getInteger("saveAirplaneModelValue",1);
        helpSkipValue = save.getBoolean("helpSkipValue",false);
        controlVersionValue = save.getInteger("controlVersionValue",1);
    }

    public int getMusicValue(){
        return musicValue;
    }

    public int getSoundValue(){
        return soundValue;
    }

    public int getVibrateValue(){
        return vibrateValue;
    }

    public int getLastPointValue(){
        return lastPointValue;
    }

    public int getSaveAirplaneModelValue() {
        return saveAirplaneModelValue;
    }

    public boolean getHelpSkipValue(){ return  helpSkipValue;}

    public int getControlVersionValue(){ return  controlVersionValue;}

    public void saveParam(int musicValue,int soundValue,int vibrateValue,int lastPointValue,int saveAirplaneModelValue,boolean helpSkipValue,int controlVersionValue){
        save.putInteger("MusicValue",musicValue);
        save.putInteger("SoundValue",soundValue);
        save.putInteger("VibrateValue",vibrateValue);
        save.putInteger("LastPointValue",lastPointValue);
        save.putInteger("saveAirplaneModelValue",saveAirplaneModelValue);
        save.putBoolean("helpSkipValue",helpSkipValue);
        save.putInteger("controlVersionValue",controlVersionValue);
        this.musicValue=musicValue;
        this.soundValue=soundValue;
        this.vibrateValue=vibrateValue;
        this.lastPointValue=lastPointValue;
        this.helpSkipValue = helpSkipValue;
        this.saveAirplaneModelValue=saveAirplaneModelValue;
        this.controlVersionValue=controlVersionValue;

        save.flush();
    }


}
